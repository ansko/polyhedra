<<<<<<< HEAD
=======
#include <cstdlib>
#include <cmath>

#include <ctype.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

>>>>>>> current_version
#include <exception>
#include <fstream>
#include <iostream>
#include <map>
#include <string>
#include <utility>
#include <vector>

#include "src/algorithms/cross_segment_plane.hpp"
#include "src/algorithms/det2.hpp"
#include "src/algorithms/det3.hpp"
#include "src/algorithms/point_inside_polygon.hpp"
#include "src/algorithms/point_inside_polyhedron.hpp"
#include "src/algorithms/polygon_crosses_plane.hpp"
#include "src/algorithms/polygon_crosses_polygon.hpp"
#include "src/algorithms/polyhedron_inside_polyhedron.hpp"
#include "src/algorithms/segment_crosses_polygon.hpp"

#include "src/constructors/binary_composite.hpp"
<<<<<<< HEAD
=======
#include "src/constructors/ternary_composite.hpp"
>>>>>>> current_version

#include "src/geometry/multipolyhedron.hpp"
#include "src/geometry/orthobrick.hpp"
#include "src/geometry/plane.hpp"
#include "src/geometry/point.hpp"
#include "src/geometry/polygon.hpp"
#include "src/geometry/polygonal_cylinder.hpp"
<<<<<<< HEAD
#include "src/geometry/polygonal_sphere.hpp"
=======
>>>>>>> current_version
#include "src/geometry/polyhedron.hpp"
#include "src/geometry/segment.hpp"


<<<<<<< HEAD
int main()
{
    srand (time(NULL));

<<<<<<< HEAD:ternary_pc.cpp
    size_t N = 12;

    Orthobrick ob(Point(-12,-12,-12), Point(12,12,12));
    PolygonalCylinder pc1(3,1,16);
    PolygonalCylinder pc2(3.1,1.2,16);

    std::vector<std::pair<TP, int> > hysto;
    hysto.push_back(std::make_pair<TP, int>(
        std::make_pair<std::shared_ptr<Polyhedron>,std::shared_ptr<Polyhedron>>(
            std::make_shared<Polyhedron>(pc1),
            std::make_shared<Polyhedron>(pc2)
        ), N));
    MultiPolyhedron ter = ternary_composite(ob, hysto);
=======
    PolygonalSphere ps;
    Orthobrick ob(Point(-10,-10,-10), Point(10,10,10));
    PolygonalCylinder pc1(2,0.5,4);
    PolygonalCylinder pc2(2,0.5,16);
    PolygonalCylinder pc3(5,0.5,3);
    std::vector<std::pair<std::shared_ptr<Polyhedron>, int> > hf;
    hf.push_back(std::make_pair<std::shared_ptr<Polyhedron>, int>(std::make_shared<Polyhedron>(pc1),10));
    hf.push_back(std::make_pair<std::shared_ptr<Polyhedron>, int>(std::make_shared<Polyhedron>(pc2),10));
    hf.push_back(std::make_pair<std::shared_ptr<Polyhedron>, int>(std::make_shared<Polyhedron>(pc3),3));
    MultiPolyhedron bin = binary_composite(ob, hf);
>>>>>>> current_version:binary_sphere.cpp

    std::ofstream ofs("test.geo");
    /*
    std::cout << "algebraic3d\n\n";
    std::cout << bin << std::endl;
    std::cout << "\ntlo filler;\ntlo matrix -transparent;\n";
    */
    ofs << "algebraic3d\n\n";
    ofs << bin << std::endl;
    ofs << "\ntlo filler;\ntlo matrix -transparent;\n";
=======
// Short name for the type to represent "Ternary Particle"
typedef std::pair<std::shared_ptr<Polyhedron>,std::shared_ptr<Polyhedron>> TP;


void print_help()
{
    std::cout << "Usage: ./ternary_pc -N 1 demo -P 16 -r 10 -R 11 -h 1 -H 2 -L 40 [-F 0.3 -I -0.3]\n";
    std::cout << "-N\tdesired number of the filler particles in the system\n";
    std::cout << "-P\tvertices number in top and bootom facets of polygonal "
                 "cylinder representing filler and matrix;\n"
                 "bigger P means begger similarity to the curlce\n";
    std::cout << "-r\tradius of the outer circumference for the filler top and bottom facets\n";
    std::cout << "-R\tradius of the outer circumference for the interface top and bottom facets"
                 "should not be less than r\n";
    std::cout << "-h\theight of the polygonal cylinder representing filler\n";
    std::cout << "-H\theight of the polygonal cylinder representing interface; "
                 "should not be less than h\n";
    std::cout << "-L\tthe size of the cubic box, representing matrix\n";
    std::cout << "-F\t[optional] maxh parameter for the filler";
    std::cout << "-I\t[optional] maxh parameter for the interface";
    std::cout << std::endl;

    return;
}


int main(int argc, char **argv)
{
    /* Generate ternary composite with filler shaped as polygonal cylinder
     */

    size_t N, P;
    float r, R, h, H, L, maxh_f=-1, maxh_i=-1;
    float ar, tau;

    srand (time(NULL));

    if (argc > 1)
      {
        char *N_value = nullptr, *P_value = nullptr,
             *r_value = nullptr, *R_value = nullptr,
             *h_value=nullptr, *H_value=nullptr,
             *L_value=nullptr,
             *maxh_f_value = nullptr, *maxh_i_value = nullptr;
        int c;
        while ((c = getopt (argc, argv, "N:P:R:r:H:h:L:I:F:")) != -1)
            switch (c)
              {
                case 'N':
                    N_value = optarg;
                    break;
                case 'P':
                    P_value = optarg;
                    break;
                case 'R':
                    R_value = optarg;
                    break;
                case 'r':
                    r_value = optarg;
                    break;
                case 'H':
                    H_value = optarg;
                    break;
                case 'h':
                    h_value = optarg;
                    break;
                case 'L':
                    L_value = optarg;
                    break;
                case 'F':
                    maxh_f_value = optarg;
                    break;
                case 'I':
                    maxh_i_value = optarg;
                    break;
                default:
                    abort ();
              }

        if (N_value == nullptr
            || P_value == nullptr 
            || r_value == nullptr
            || R_value == nullptr
            || h_value == nullptr
            || H_value == nullptr
            || L_value == nullptr)
          {
            std::cout << "Some required options are not set, exiting\n";
            std::cout << "\tN_value missing? " << (N_value == nullptr) << std::endl;
            std::cout << "\tP_value missing? " << (P_value == nullptr) << std::endl;
            std::cout << "\tr_value missing? " << (r_value == nullptr) << std::endl;
            std::cout << "\tR_value missing? " << (R_value == nullptr) << std::endl;
            std::cout << "\th_value missing? " << (h_value == nullptr) << std::endl;
            std::cout << "\tH_value missing? " << (H_value == nullptr) << std::endl;
            std::cout << "\tL_value missing? " << (L_value == nullptr) << std::endl;

            print_help();
            return 0;
          }

        N = atoi(N_value);
        P = atoi(P_value);
        r = atof(r_value);
        R = atof(R_value);
        h = atof(h_value);
        H = atof(H_value);
        L = atof(L_value);

        if (H < h)
          {
            std::cout << "Error, H < h: " << H << " < " << h << ", exiting\n";
            return 0;
          }
        if (R < r)
          {
            std::cout << "Error, R < r: " << R << " < " << r << ", exiting\n";
            return 0;
          }

        if (maxh_f_value != nullptr)
            maxh_f = atof(maxh_f_value);
        if (maxh_i_value != nullptr)
            maxh_i = atof(maxh_i_value);
      }
    else  // stub
      {
        print_help();
        return 0;
      }

    MultiPolyhedron ter;
    try
      {
        Orthobrick ob(Point(-L/2,-L/2,-L/2), Point(L/2,L/2,L/2));
        PolygonalCylinder pc1(r, h, P);
        PolygonalCylinder pc2(R, H, P);

        std::vector<std::pair<TP, int> > hysto;
        hysto.push_back(std::make_pair<TP, int>(
            std::make_pair<std::shared_ptr<Polyhedron>,std::shared_ptr<Polyhedron>>(
                std::make_shared<Polyhedron>(pc1),
                std::make_shared<Polyhedron>(pc2)
            ), N));
        ter = ternary_composite(ob, hysto);
      }
    catch (std::string e)
      {
        std::cout << e << std::endl;
        return 0;
      }

    std::ofstream ofs("test.geo");
    ofs << "algebraic3d\n\n";
    output_ternary_system_every_particle(ofs, ter);
    if (maxh_f > 0)
        ofs << "\ntlo filler -maxh=" << maxh_f << ";";
    else
        ofs << "\ntlo filler;";
    if (maxh_i > 0)
        ofs << "\ntlo interface -transparent -maxh=" << maxh_i << ";";
    else
        ofs << "\ntlo interface -transparent;";
    ofs << "\ntlo matrix -transparent;\n";

    {
        float central_angle = 2*M_PI / P;
        float fi_appro = N * 0.5*P*r*r*sin(central_angle) * h / (L*L*L);
        std::cout << "fi_f should be about " << fi_appro << std::endl;
    }

    ar = 2*r/h;
    tau = (H - h)/2/h;
    std::cout << "AR = " << ar << std::endl;
    std::cout << "tau = " << tau << std::endl;
    std::cout << "Particles appended: "
              << ter.phases_content()["filler"].size() << " of " << N
              << std::endl;
    if (maxh_f > 0)
        std::cout << "maxh_f = " << maxh_f << std::endl;
    else
        std::cout << "maxh_f not set\n";
    if (maxh_i > 0)
        std::cout << "maxh_i = " << maxh_i << std::endl;
    else
        std::cout << "maxh_i not set\n";

>>>>>>> current_version

    return 0;
}

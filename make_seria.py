#!/usr/bin/env python3


import shutil
import subprocess
import sys
import time


def main(ar, tau):
    multip = 3

    h = 1
    r = h * ar/2
    H = h * (1 + 2*tau)
    R = r + tau*h
    F = h/2
    I = H/2

    L = (3.14 * r**2 * h * 6*multip / 0.03)**(1/3)
    L = (round(L/10)) * 10
    print(L)

    for idx in range(1, 7):
        N = multip * idx
        subprocess.call(["./ternary_pc",  "-P16", f"-r{r}", f"-R{R}", f"-h{h}",
                         f"-H{H}", f"-L{L}", f"-N{N}", f"-F{F}", f"-I{I}"])
        shutil.move("test.geo", f"ar{ar}_tau{tau}_F{F}_I{I}_N{N}_1.geo")
        time.sleep(1)
        subprocess.call(["./ternary_pc",  "-P16", f"-r{r}", f"-R{R}", f"-h{h}",
                         f"-H{H}", f"-L{L}", f"-N{N}", f"-F{F}", f"-I{I}"])
        shutil.move("test.geo", f"ar{ar}_tau{tau}_F{F}_I{I}_N{N}_2.geo")
        time.sleep(1)
        subprocess.call(["./ternary_pc",  "-P16", f"-r{r}", f"-R{R}", f"-h{h}",
                         f"-H{H}", f"-L{L}", f"-N{N}", f"-F{F}", f"-I{I}"])
        shutil.move("test.geo", f"ar{ar}_tau{tau}_F{F}_I{I}_N{N}_3.geo")


if __name__ == '__main__':
    if len(sys.argv) < 3:
        print("./exe ar tau")
    else:
        main(ar=float(sys.argv[1]), tau=float(sys.argv[2]))

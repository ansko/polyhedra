#ifndef SRC_ALGORITHMS_DET2
#define SRC_ALGORITHMS_DET2


// Calculation of the determinant for the matrices of size 2x2
// which is defined by 4 floats. The method is dummy but fast.
const float det2(const float a11, const float a12,
                 const float a21, const float a22)
{
    return a11*a22 - a21*a12;
}


#endif  // #ifndef SRC_ALGORITHMS_DET2

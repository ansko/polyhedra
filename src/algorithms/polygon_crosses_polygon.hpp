#ifndef SRC_ALGORITHMS_POLYGON_CROSSES_POLYGON_HPP
#define SRC_ALGORITHMS_POLYGON_CROSSES_POLYGON_HPP


#include "../geometry/segment.hpp"
#include "../geometry/polygon.hpp"

#include "polygon_crosses_plane.hpp"
#include "segment_crosses_polygon.hpp"


bool polygon_crosses_polygon(const Polygon& poly_a, const Polygon& poly_b)
{
    // Each polygon crosses plane where other lies
    Plane pl_a({poly_a.center(), poly_a.vertices()[0], poly_a.vertices()[1]});
    Plane pl_b({poly_b.center(), poly_b.vertices()[0], poly_b.vertices()[1]});
    if (!polygon_crosses_plane(poly_a, pl_b))
        return false;
    if (!polygon_crosses_plane(poly_b, pl_a))
        return false;

    // Any edge of a crosses b
    for (size_t idx = 0; idx < poly_a.vertices().size(); ++idx)
      {
        size_t idx_a, idx_b;
        if (idx < poly_a.vertices().size() - 1)
          {
            idx_a = idx;
            idx_b = idx + 1;
          }
        else
          {
            idx_a = idx;
            idx_b = 0;
          }
        Segment seg(poly_a.vertices()[idx_a], poly_a.vertices()[idx_b]);
        if (segment_crosses_polygon(seg, poly_b))
            return true;
      }
    return false;
}


#endif  // #ifndef SRC_ALGORITHMS_POLYGON_CROSSES_POLYGON_HPP

#ifndef SRC_ALGORITHMS_DET3
#define SRC_ALGORITHMS_DET3


// Calculation of the determinant for the matrices of size 3x3
// which is defined by 9 floats. The method is dummy but fast.
const float det3(const float a11, const float a12, const float a13,
                 const float a21, const float a22, const float a23,
                 const float a31, const float a32, const float a33)
{
    float d = 0;
    d += a11 * (a22*a33 - a32*a23);
    d -= a12 * (a21*a33 - a31*a23);
    d += a13 * (a21*a32 - a31*a22);

    return d;
}


#endif  // #ifndef SRC_ALGORITHMS_DETS3

#ifndef SRC_ALGORITHMS_CROSS_SEGMENT_PLANE_HPP
#define SRC_ALGORITHMS_CROSS_SEGMENT_PLANE_HPP


#include "../geometry/plane.hpp"
#include "../geometry/point.hpp"
#include "../geometry/segment.hpp"

#include "det3.hpp"


std::shared_ptr<Point> cross_segment_plane(const Segment &seg, const Plane &pl)
{
    float det_beg = det3(seg.pt_beg().x()-pl.pt_0().x(), seg.pt_beg().y()-pl.pt_0().y(), seg.pt_beg().z()-pl.pt_0().z(),
                         seg.pt_beg().x()-pl.pt_1().x(), seg.pt_beg().y()-pl.pt_1().y(), seg.pt_beg().z()-pl.pt_1().z(),
                         seg.pt_beg().x()-pl.pt_2().x(), seg.pt_beg().y()-pl.pt_2().y(), seg.pt_beg().z()-pl.pt_2().z());
    float det_end = det3(seg.pt_end().x()-pl.pt_0().x(), seg.pt_end().y()-pl.pt_0().y(), seg.pt_end().z()-pl.pt_0().z(),
                         seg.pt_end().x()-pl.pt_1().x(), seg.pt_end().y()-pl.pt_1().y(), seg.pt_end().z()-pl.pt_1().z(),
                         seg.pt_end().x()-pl.pt_2().x(), seg.pt_end().y()-pl.pt_2().y(), seg.pt_end().z()-pl.pt_2().z());

    if (det_beg * det_end > 0)
        return nullptr;

    float c = std::fabs(det_beg) / (std::fabs(det_beg) + std::fabs(det_end));
    float x = seg.pt_beg().x() + c*(seg.pt_end().x() - seg.pt_beg().x());
    float y = seg.pt_beg().y() + c*(seg.pt_end().y() - seg.pt_beg().y());
    float z = seg.pt_beg().z() + c*(seg.pt_end().z() - seg.pt_beg().z());

    return std::make_shared<Point>(x,y,z);
}


#endif  // #ifndef SRC_ALGORITHMS_CROSS_SEGMENT_PLANE_HPP

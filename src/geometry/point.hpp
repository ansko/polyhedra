#ifndef SRC_GEOMETRY_POINT_HPP
#define SRC_GEOMETRY_POINT_HPP


#include <cmath>
#include <iostream>
#include <memory>
#include <string>


class Point
{
public:
    Point(const float x, const float y, const float z)
    : _x(x), _y(y), _z(z) {}

    float x() const { return _x; }
    float y() const { return _y; }
    float z() const { return _z; }

    void translate(const float dx, const float dy, const float dz)
      {
        _x += dx;
        _y += dy;
        _z += dz;
      };

    void rotate(const Point &rot_c, const float ax, const float ay, const float az)
      {
        float dx = _x - rot_c.x();
        float dy = _y - rot_c.y();
        float dz = _z - rot_c.z();

        // ax:
        // 1 0 0
        // 0 c -s
        // 0 s c
        float tmp_dx = dx;
        float tmp_dy = cos(ax)*dy - sin(ax)*dz;
        float tmp_dz = sin(ax)*dy + cos(ax)*dz;
        dx = tmp_dx;
        dy = tmp_dy;
        dz = tmp_dz;

        // ay
        // c 0 s
        // 0 1 0
        // -s 0 c
        tmp_dx = cos(ay)*dx + sin(ay)*dz;
        tmp_dy = dy;
        tmp_dz = -sin(ay)*dx + cos(ay)*dz;
        dx = tmp_dx;
        dy = tmp_dy;
        dz = tmp_dz;

        // az
        // c -s 0
        // s c 0
        // 0 0 1
        tmp_dx = cos(az)*dx - sin(az)*dy;
        tmp_dy = sin(az)*dx + cos(az)*dy;
        tmp_dz = dz;

        _x = rot_c.x() + dx;
        _y = rot_c.y() + dy;
        _z = rot_c.z() + dz;
      }

private:
    float _x;
    float _y;
    float _z;
};


// CSG-ready (even though point primitive is absent in netgen,
// it may be used as part of other primitives' outputs)
std::ostream& operator<<(std::ostream& os, const Point& pt)
{
    os << pt.x() << ", " << pt.y() << ", " << pt.z();
    return os;
}


#endif  // #ifndef SRC_GEOMETRY_POINT_HPP

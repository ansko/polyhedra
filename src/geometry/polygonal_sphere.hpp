#ifndef SRC_GEOMETRY_POLYGONAL_SPHERE_HPP
#define SRC_GEOMETRY_POLYGONAL_SPHERE_HPP

#include <cmath>
#include <vector>

#include "../geometry/point.hpp"
#include "../geometry/plane.hpp"


class PolygonalSphere
{
public:
    PolygonalSphere(Point center, float R, size_t N)
    : _center(center)
      {
        for (size_t idx_theta = 0; idx_theta < N; ++idx_theta)
            for (size_t idx_phi = 0; idx_phi < N; ++idx_phi)
              {
                float theta = M_PI * idx_theta/N;
                float theta_prev = M_PI * (idx_theta-1)/N;
                float phi = 2*M_PI * idx_phi/N;
                float phi_prev = 2*M_PI * (idx_phi-1)/N;

                Point pt1 = Point(R*sin(theta)*cos(phi_prev),
                                  R*sin(theta)*sin(phi_prev),
                                  R*cos(theta));
                Point pt2 = Point(R*sin(theta_prev)*cos(phi),
                                  R*sin(theta_prev)*sin(phi),
                                  R*cos(theta_prev));
                Point pt3 = Point(R*sin(theta)*cos(phi),
                                  R*sin(theta)*sin(phi),
                                  R*cos(theta));
                _facet_planes.emplace_back(pt1, pt2, pt3);
              }
      }

    const std::vector<Plane> facet_planes() const { return _facet_planes; }
    const Point center() const { return _center; }
private:
    std::vector<Plane> _facet_planes;
    Point _center;
};


// CSG-consistent via planes (each polygon is printed as plane where it lies)
std::ostream& operator<<(std::ostream& os, const PolygonalSphere& ps)
{
    const Point c = ps.center();

    for(size_t idx = 0; idx < ps.facet_planes().size(); ++idx)
      {
        if (idx != 0)
            os << " and ";
        os << ps.facet_planes()[idx];
      }

    return os;
}


#endif  // #ifndef SRC_GEOMETRY_POLYGONAL_SPHERE_HPP

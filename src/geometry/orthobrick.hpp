#ifndef SRC_GEOMETRY_ORTHOBRICK_HPP
#define SRC_GEOMETRY_ORTHOBRICK_HPP


#include <string>

#include "polyhedron.hpp"


class Orthobrick : public Polyhedron
{
public:
    Orthobrick(const Point& min_pt, const Point& max_pt)
    : _min_pt(min_pt), _max_pt(max_pt)
    {
      if (   min_pt.x() >= max_pt.x()
          || min_pt.y() >= max_pt.y()
          || min_pt.z() >= max_pt.z())
          throw std::string("Exception in Orthobrick::Orthobrick(const Point&, const Point&): "
                            "minimum is not less than maximum!");

      const Point a000 = min_pt;
      const Point a001 = Point(min_pt.x(), min_pt.y(), max_pt.z());
      const Point a010 = Point(min_pt.x(), max_pt.y(), min_pt.z());
      const Point a011 = Point(min_pt.x(), max_pt.y(), max_pt.z());
      const Point a100 = Point(max_pt.x(), min_pt.y(), min_pt.z());
      const Point a101 = Point(max_pt.x(), min_pt.y(), max_pt.z());
      const Point a110 = Point(max_pt.x(), max_pt.y(), min_pt.z());
      const Point a111 = max_pt;

      const Polygon left = Polygon({a000, a001, a011, a010});
      const Polygon right = Polygon({a100, a101, a111, a110});
      const Polygon top = Polygon({a001, a011, a111, a101});
      const Polygon bottom = Polygon({a000, a010, a110, a100});
      const Polygon forward = Polygon({a010, a011, a111, a110});
      const Polygon backward = Polygon({a000, a001, a101, a100});

      _facets = {left, right, top, bottom, forward, backward};
    }

    const Point min_pt() const { return _min_pt; }
    const Point max_pt() const { return _max_pt; }
private:
    Point _min_pt;
    Point _max_pt;
};


// CSG-consistent via planes (each polygon is printed as plane where it lies)
std::ostream& operator<<(std::ostream& os, const Orthobrick& ob)
{
    os << "orthobrick(" << ob.min_pt() << "; " << ob.max_pt() << ")";
    return os;
}
#endif  // #ifndef SRC_GEOMETRY_ORTHOBRICK_HPP
